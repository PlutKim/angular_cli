import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthenticationComponent } from './services/authentication/authentication.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    AuthenticationComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FooterComponent, HeaderComponent
  ]
})
export class CoreModule { }
