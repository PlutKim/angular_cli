import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './components/product/product.component';
import { ProductsComponent } from './products.component';
import {ButtonModule} from "../../shared/button/button.module";
import {InputModule} from "../../shared/input/input.module";



@NgModule({
  declarations: [
    ProductComponent,
    ProductsComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    InputModule
  ],
  exports: [
    ProductsComponent
  ]
})
export class ProductsModule { }
