import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {CoreModule} from "./core/core.module";
import { ButtonComponent } from './shared/button/button.component';
import { InputComponent } from './shared/input/input.component';
import {CartModule} from "./features/cart/cart.module";
import {ProductsModule} from "./features/products/products.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, CoreModule, CartModule, ProductsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
